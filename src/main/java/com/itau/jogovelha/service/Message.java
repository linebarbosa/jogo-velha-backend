package com.itau.jogovelha.service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Message {
	private final static String QUEUE_NAME = "logs_received";
	
	public static void sendMessage(String message) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		String QUEUE_SERVER = System.getenv("QUEUE_SERVER");
		factory.setHost(QUEUE_SERVER);
		factory.setPort(5672);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, true, false, false, null);
		channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
		System.out.println(" [x] Sent '" + message + "'");

		channel.close();
		connection.close();
	}

}
