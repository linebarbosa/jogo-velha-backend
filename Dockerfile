FROM fedora:28
RUN dnf install -y java-1.8.0-openjdk
COPY ./jogo-velha-backend/target/jogovelha-0.0.1-SNAPSHOT.jar /app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
